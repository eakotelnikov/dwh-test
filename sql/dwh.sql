-- Live Coding DB
-- -------------------------------------------------------------

-- Roles
CREATE ROLE lc_role NOSUPERUSER NOLOGIN;
COMMENT ON ROLE lc_role IS 'The role, which is used for all activities in the Live Coding database.';

-- Accounts
CREATE USER dba_sys WITH ENCRYPTED PASSWORD '***' SUPERUSER NOCREATEDB CREATEROLE LOGIN;
CREATE USER asmith WITH ENCRYPTED PASSWORD '***' IN ROLE lc_role;

-- Tablespaces
-- postgres@hostname ~]$ mkdir -p /data/db/postgres/pgdata/pgtblspc/lc_db/system
-- postgres@hostname ~]$ mkdir -p /data/db/postgres/pgdata/pgtblspc/lc_db/data
-- postgres@hostname ~]$ mkdir -p /data/db/postgres/pgdata/pgtblspc/lc_db/data_idx

CREATE TABLESPACE lc_system OWNER dba_sys LOCATION '/data/db/postgres/pgdata/pgtblspc/lc_db/system';
COMMENT ON TABLESPACE lc_system IS 'The system tablespace for the Live Coding database';

CREATE TABLESPACE lc_data OWNER dba_sys LOCATION '/data/db/postgres/pgdata/pgtblspc/lc_db/data';
COMMENT ON TABLESPACE lc_data IS 'The data tablespace for the Live Coding database';

CREATE TABLESPACE lc_data_idx OWNER dba_sys LOCATION '/data/db/postgres/pgdata/pgtblspc/lc_db/data_idx';
COMMENT ON TABLESPACE lc_data_idx IS 'The data indexes tablespace for the Live Coding database';

-- Set default search pathes
ALTER ROLE lc_role SET search_path = public;

-- Set default tablespace
ALTER ROLE lc_role SET default_tablespace = 'lc_data';

-- Database
CREATE DATABASE lc_db WITH OWNER=dba_sys ENCODING='UTF8' TABLESPACE=lc_system;
COMMENT ON DATABASE lc_db is 'Live Coding Database';

-- Permissions
GRANT ALL ON TABLESPACE lc_data TO lc_role;
GRANT ALL ON TABLESPACE lc_data_idx TO lc_role;
